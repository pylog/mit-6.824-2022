---
title: MIT 6.824 distributed systems 2022
mathjax: false
date: 2024-01-27 20:00
---

<p><center>“沉舟侧畔千帆过，病树前头万木春”</center></p>

<!--more-->

### 前言

首先，本文的主旨是，分享一项我真实写在秋招简历上的东西，分布式存储系统，从代码的角度而非知识点。相比于二手的总结，我认为原创的代码实现更具分享价值，应具有获取第一手知识的追求，从视频、板书和论文中。其次，[6.824](http://nil.csail.mit.edu/6.824/2022/) 已于 2023 更名为 [6.5840](http://nil.csail.mit.edu/6.5840/2024/)，且于 2024 增加了 1 个 lab 和更新了原 lab 们的测试样例，当前的分享已略显过时。最后，因为我还有毕设要写，就不跟进了，又因为课程页要求的是不要公开当前版本的代码，所以我少了公开旧版仓库的心理负担，见 <https://gitee.com/pylog/mit-6.824-2022>，欢迎 start


### 项目介绍

#### 简历版

麻省理工研究生课程项目，在提供的 `Go` 语言代码框架上，复刻论文细节，实现完整、容错、分片的分布式存储系统，并通过包括不可靠网络环境、服务器崩溃、客户端重启和 RPC 次数限制在内的全部测试样例

1. 实现 Raft 协议，完成有领导选举、日志复制分发、持久化存储和状态机快照等功能
2. 实现 Fault-tolerant Key/Value Service，服务器集群底层使用 Raft 保证其线性强一致性，维护一个简易的数据库，客户端能对其中的键值对进行读写和追加操作
3. 实现 Sharded Key/Value Service，将单一数据库分裂到多个 Service Group，每个 Group 可存储多个 Sharded DB，Group 内部使用 Raft 容错，Group 之间根据配置传输 Shard，使数据负载均衡，以减轻存储压力，提高响应效率
4. 实现 MapReduce 并行计算框架，中心 Coordinator 节点划分总任务，先后进入 Map 和 Reduce 阶段，期间监听多个 Worker 节点并为其分布子任务；Worker 利用各自的本地资源执行任务，反馈结果给中央文件系统


#### 大白话版

- lab 1：MapReduce 框架，供 mrapp（如词频统计、链接引用计数）使用，是一主多从的设计。实验提供了 sequential 版的 mr，载入 mrapp，将其运行结果与要求实现的 distributed 版 mr 的对比，全一致则通过测试。是分布式的体验级任务。
- lab 2：Raft 协议，将论文中除集群成员主动变更外的全部功能，落实到代码上。raft 通过保存具体操作到 log 的方式，保持其上层状态机的执行顺序，通过同步 log，保证集群的线性强一致性。与 mr 不同，raft 是去中心化的，任何节点都有成为 leader 的可能。需要为容错和恢复实现持久化，为日志压缩实现快照。困难重重又无比重要，是后面实验的基建。
- lab 3：Fault-tolerant Key/Value Service，将 raft 与上层状态机结合起来，实现单个服务器集群和能与之交互的客户端，能应用，才是正式入门分布式。
- lab 4：Sharded Key/Value Service，也是一主多从的设计，只不过每个节点都是一个 raft 集群而非单一的状态机。4A 实现主节点 shardctrler 集群 ，负责 config 的查询和更新，config 记录了当前有多少个 group 和每个 group 负责哪些 shard，通过修改 config 间接影响从节点 shardkv；4B 实现 shardkv，记 shardkv 集群为 group，通过周期性查询 config 并传输 shard 的方式实现数据库的分片  


### 目录结构

- lab 1 - main：载入 mrapp 并测试；mr；mrapp
- lab 2 - raft
- pylog：为 lab 3、4 提供通用模板
- lab 3 - kvraft：直接实现；kvraft_impl2：模板实现
- lab 4 - shardctrler；shardkv
- helper
   * labrpc：在 lab 2、3、4 中使用的 rpc 包，因为测试样例需要模拟不同的网络环境
   * labgob：labrpc 所依赖的传输格式，封装了 encoding/gob，gob 格式是专为 go 语言设计的；结构体需在注册后才可被编解码
   * porcupine：在 lab 3、4 中使用的可视化包，用于检测线性一致性；在出现错误时将导出网页，有帮助，但不大
   * models：启用 porcupine


### 后来者优化

1. 使用最新的 [6.5840](https://pdos.csail.mit.edu/6.5840/)
2. 使用 [Debugging by Pretty Printing](https://blog.josejg.com/debugging-pretty/)
3. Speed Up，lab 3 TestSpeed3A 在我的机器上，使用 -race flag 时，impl_1 需要 29s，impl_2 需要 43s，不使用 -race flag 时均仅需 4s，差距过大
4. 为 lab 3、4，实现 Read-only operations，不作要求，也不好实现


### 面试杂谈

现在是 24 届春招和 25 届找实习的时间点，期望本文能有所帮助吧。此项目偏学术而非生产，是否写入简历需谨慎考虑。  
如果是往分布式、存储的方向发展，那么本项目是必修的，即有不一定加分，没有必扣分；如果是往 go 开发的方向发展，那么本项目可作为八股文触发器；如果是其他方向，我只能保证这是一个很有难度、很能体现自学水平的学业项目，但我不能肯定这是一个合适的简历项目，看公司规模、看岗位要求、看面试官的偏好。  
我自己遇到的面试官，有表示认可的，也有直言技术栈不匹配的（原话，“不如xx管理系统，更希望应聘者有后端相关的实操经验”），但没有表示感到惊艳的。  
如果决定写入简历，要经得起问！真正完成了 lab 4 的人其实很少，我见过几份写相同项目的简历，只需看看介绍就能判断出来。  
面试官不一定了解分布式，也不一定有项目拷打环节。如若被问，细节少、视野多。细节集中在 raft 的机制上，如选举的过程、心跳的作用，或者问 rpc 的原理和应用；视野又大致分为两类，raft 被应用在哪些产品中，和是否知道其他分布式相关的协议，它们与 raft 的区别。只要有看课程视频，都是能回答上来的，安心。  

